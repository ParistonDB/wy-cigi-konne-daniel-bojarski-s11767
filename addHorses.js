var mongoose = require('mongoose');
var judges                    = require('./routes/models/judge');
var async                     = require('async');
var horses                    = require('./routes/models/horse');
mongoose.connect('mongodb://localhost/wyscigikonne');

var horse1 = new horses({ _id: mongoose.Types.ObjectId(), name: "Plotka"});
var horse2 = new horses({ _id: mongoose.Types.ObjectId(), name: "Strzala"});
var horse3 = new horses({ _id: mongoose.Types.ObjectId(), name: "Wicher"});
var horse4 = new horses({ _id: mongoose.Types.ObjectId(), name: "Tornado"});
var horse5 = new horses({ _id: mongoose.Types.ObjectId(), name: "Brego"});

var addHorse = function(horse) {
  return function(done) {
    horses.findOne({
      name: horse.name
    }, function(error, result) {
      if(error) console.log(error);
      else {
        if(result === null) {
          horse.save(function(error) {
            if(!error) done();
          });
        }
      }
    });
  };
};

async.series([
  addHorse(horse1),
  addHorse(horse2),
  addHorse(horse3),
  addHorse(horse4),
  addHorse(horse5),
  function a(done) {
    for(i = 1; i <= 20; i++) {
      new judges({
        _id: mongoose.Types.ObjectId(),
        identyfikator: Math.floor((Math.random() * (9999 - 1000 + 1)) + 1000),
        active: false
      }).save();
    }

    judges.find(function(error, result) {
      if(error) console.log(error);
      if(result !== null) {
        if(result.length === 20) {
          done();
        }
      }
    });
  }
], function(error) { if(!error) process.exit(); });
