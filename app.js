var express                   = require('express'),
    path                      = require('path'),
    favicon                   = require('serve-favicon'),
    logger                    = require('morgan'),
    cookieParser              = require('cookie-parser'),
    bodyParser                = require('body-parser'),
    ejs                       = require('ejs'),
    app                       = express(),
    httpServer                = require("http").Server(app),
    io                        = require("socket.io")(httpServer),
    routes                    = require('./routes/index'),
    users                     = require('./routes/users'),
    judge                     = require('./routes/judge'),
    horses                    = require('./routes/models/horse'),
    judges                    = require('./routes/models/judge'),
    async                     = require('async'),
    _                         = require('underscore'),
    mongoose                  = require('mongoose'),
    lessMiddleware            = require('less-middleware');

mongoose.connect('mongodb://localhost/wyscigikonne');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(lessMiddleware('/less', {
  dest: '/css',
  pathRoot: path.join(__dirname, 'public')
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules')));
app.use('/lib/', express.static(__dirname + '/bower_components/jquery/dist/'));

app.use('/', routes);
app.use('/users', users);
app.use('/judge', judge);


var sortedFinalRanking = [];
var isRace = false;
var socketsIds = [];                    //tablica zawierajaca wszystkie ID socketow (urzadzen sedziowskich), ktore skladowane sa dla kazdego, kto wejdzie na podstrone /judge
var activeHorses = [];                  //tablica zawierajaca liste wszystkich koni, ktore biora udzial w aktualnym biegu
var activeLeftHorses = [];              //tablica zawierajaca liste POZOSTALYCH koni, ktore biora udzial w aktualnym biegu
var ie = {
  judgeMarks: 0,
  actualHorse: 0,
};
var markedHorses = [];                  //tablica zawierajaca konie, ktore ukonczyly swoj bieg
var horseCount = 0;                     //lista koni, ktora jest ustalana losowo (z zakresu od 2 do ilosc koni w bazie danych)
var activeJudges = [];                  //tablica zawierajaca liste wszystkich sedziujacych sedziow
var judgesCount = 0;                    //AKTUALNIE NIEUZYWANA
var judgeAgreements = 0;                //licznik potwierdzen przez wylosowanych sedziow, ze chca sedziowac (jezeli wartosc = ilosc wylosowanych sedziow), to bieg sie rozpoczyna
var activeKeys = [];                    //tablica zawierajaca unikalne, wylosowane z bazy danych klucze, ktorych podanie uprawnia sedziow do korzystania z panelu sedziowskiego
var judgeMarks = [];                    //tablica zawierajaca oceny wszystkich sedziow dla danego konia
var finalRanking = [];
/*
  Po skonczonym biegu konia (jezeli nie jest ostatnim), wszystkie powyzsze zmienne poza "socketsIds" zostaja zresetowane.
  Przypominajka: Zmien pozniej wartosci ilosci losowanych kluczy i sedziow
*/

io.on('connection', function(socket) {
  var history = {
    isRace: isRace,
    activeHorses: activeLeftHorses,
    judgeMarks: judgeMarks,
    actualHorse: activeLeftHorses[0],
    finalRanking: sortedFinalRanking,
  };

  socket.emit('history', history);

  socket.on('race start', function(data) {
      if(data.start === true) {
        horses.find(function(error, result) {
          if(error) console.log(error);
          else {
            isRace = true;
            horseCount = Math.floor((Math.random() * (result.length - 2 + 1)) + 2);
            judgesCount = Math.floor((Math.random() * (socketsIds.length - 3 + 1)) + 3);
            activeHorses = _.sample(result, horseCount);  //lista koni bioracych udzial w biegu
            activeLeftHorses = activeHorses.slice();

            //funkcja losujaca sedziow (wykonywana przed ocenianiem kazdego konia)
            randomJudges();
          }
        });
      }
  });

  socket.on('judgeMarks', function(data) {
    /*
      Ponizsza petla sluzy za zabezpieczenie, ktore sprawdza czy id sedziego istnieje juz w wynikach (oznacza to, ze juz zaglosowal).
      Jezeli tak, to usuwa jego ocene i wstawia nowa. Zapobiega to dublowaniu ocen wprowadzonych przez jednego sedziego.
      Zabezpieczenie to istnieje, zeby umozliwic sedziom aktualizacje ocen, kiedy panel sedziowski nie jest jeszcze zablokowany.
    */
    for(var i = 0; i < judgeMarks.length; i++) {
      if(judgeMarks[i].judge === socket.id) {
        judgeMarks.splice(judgeMarks.indexOf(judgeMarks[i]), 1);
      }
    }
    data.judge = socket.id;
    judgeMarks.push(data);

    //Jezeli dlugosc tablicy trzymajacej oceny sedziow = liczba aktywnych sedziow, to procecs oceniania danego konia konczy sie.
    if(judgeMarks.length >= activeJudges.length) {

      var judgeMarksContainer = {
          horseID: null,
          judgesCount: 0,
          T: 0,
          G: 0,
          K: 0,
          N: 0,
          R: 0
      };

      for(var index = 0; index < judgeMarks.length; index++) {
        judgeMarksContainer.horseID = judgeMarks[index].horseID;
        judgeMarksContainer.T += judgeMarks[index].T;
        judgeMarksContainer.G += judgeMarks[index].G;
        judgeMarksContainer.K += judgeMarks[index].K;
        judgeMarksContainer.N += judgeMarks[index].N;
        judgeMarksContainer.R += judgeMarks[index].R;
      }

      horses.findOneAndUpdate({
        _id: judgeMarksContainer.horseID
      }, {
        judgesCount: activeJudges.length,
        T: judgeMarksContainer.T,
        G: judgeMarksContainer.G,
        K: judgeMarksContainer.K,
        N: judgeMarksContainer.N,
        R: judgeMarksContainer.R
      }, function(error) {
        if(error) console.log(error);
        else {
          console.log("Koniec biegu dla tego konia");
          judgeAgreements = 0;
          judgesCount = 0;
          activeJudges = [];
          judgeMarks = [];
          _.map(socketsIds, function(socket) { io.sockets.connected[socket].agree = false; });

          async.parallel([
            getHorse(activeLeftHorses[0]),
            function a(done) {
              activeLeftHorses.splice(activeLeftHorses[0], 1);
              //wysylanie informacji do nadzorujacego wysci /racestart.html
              var raceInfo = {
                activeHorses: activeHorses.length,
                activeLeftHorses: activeLeftHorses.length
              };
              io.emit('race info', raceInfo);
              done();
            },
            function b(done) {
              io.emit('clear');
              if(activeLeftHorses.length > 0) {
                randomJudges();
              } else {
                io.emit('let horse unlock');
                io.emit('let start unlock');
                isRace = false;
              }
            }
          ]);
        }
      });

    }

    ie.judgeMarks = judgeMarks;
    io.emit('last marks', ie);
  });

  /*
    Urzadzenia sa automatycznie wychwytywane, kiedy uzytkownik jest na podstronie /judge. ID socketa, ktory sie utworzy jest umieszczany w tabeli "socketsIds",
    ktora sluzy za pule, z ktorej beda losowani sedziowie.
  */
  socket.on('judgePage', function() {
    socketsIds.push(socket.id);
  });

  /*
    Kazdy sedzia, ktory zostanie wylosowany z tabeli socketsIds (tabeli, ktora trzyma ID wszystkich polaczen i z ktorej losowani sa sedziowie dla danego konia),
    musi potwierdzic chec sedziowania. Jezeli ilosc potwierdzen (licznik trzymany w zmiennej "judgeAgreements") = ilosc wylosowanych sedziow, to kazdy z wylosowanych
    otrzymuje emit od serwera, w ktorym pojawia sie formularz do wpisania uniwersalnego klucza.
  */
  socket.on('judgeDecline', function() {
    activeJudges.splice(activeJudges.indexOf(socket.id), 1);
    //if(_.contains(finalRanking, result))
    randomJudges();
    console.log("Sedzia odmowil - wyrzucam go.");
  });

  socket.on('prepare yourself', function() {
    var horseIndex = activeHorses.indexOf(activeLeftHorses[0]) + 1;
    for(var i = 0; i < activeJudges.length; i++) {
      io.to(activeJudges[i]).emit('prepare yourself', horseIndex);
    }
    //drugowanie listy koni bioracych udzial w danym biegu dla widzow (strona glowna)
    io.emit('print horses', activeLeftHorses);
    ie.actualHorse = activeLeftHorses[0];
    ie.judgeMarks = judgeMarks;
    io.emit('last marks', ie);
  });

  socket.on('let horse', function() {
    var judgeInfo = {
      key: 0,
      horseNumber: 0,
      horseID: 0
    };

    //w tabeli "judges" trzymane jest n kluczy, ktore sa rozdawane indywidualnie kazdemu wylosowanemu sedziowi.
    judges.find({

    }, function(error, result) {
      if(error) console.log(error);
      else {
          activeKeys = _.sample(result, activeJudges.length); //losowanie kluczy
          for(var i = 0; i < activeJudges.length; i++) {
            //kazdy z sedziow dostaje uniwersalny klucz
              judgeInfo.key = activeKeys[i];
              judgeInfo.horseNumber = activeHorses.indexOf(activeLeftHorses[0]) + 1;
              judgeInfo.horseID = activeLeftHorses[0]._id;
          }
          io.to(socket.id).emit('judge table', judgeInfo);
      }
    });
  });

  socket.on('judgeAgreement', function() {
    judgeAgreements++;
    socket.agree = true;

    if(judgeAgreements === activeJudges.length) {
      io.emit('let horse unlock');
    }
  });

  socket.on('user connected', function() {
    socket.emit('user connected', 'User connected');
  });

  socket.on('disconnect', function() {
    socketsIds.splice(socketsIds.indexOf(socket.id), 1);
  });

  var randomJudges = function() {
    //Losowanie sedziow
    //activeJudges = _.sample(socketsIds, 3 - activeJudges.length);
    _.map(_.sample(_.difference(socketsIds, activeJudges), 3 - activeJudges.length), function(num) {
      activeJudges.push(num);
    });

    for(var i = 0; i < activeJudges.length; i++) {
      //emituje wiadomosc do wylosowanych sedziow (pokazuje i chowam im elementy)
      if(io.sockets.connected[activeJudges[i]].agree !== true) io.to(activeJudges[i]).emit('judge');
    }
    io.emit('waitingForJudges');
  };
});

var getHorse = function(horse) {
  return function(done) {
    horses.findOne({
      _id: horse.id
    }, function(error, result) {
      if(error) done(error);
      else {
        if(result !== null) {
          finalRanking.push(result);
          if(_.contains(finalRanking, result)) {
            sortedFinalRanking = finalRanking.sort(function(a, b) {
              if((a.T + a.G + a.K + a.N + a.R)/a.i > (b.T + b.G + b.K + b.N + b.R)/b.i) {
                return 1;
              }

              if((a.T + a.G + a.K + a.N + a.R)/a.i < (b.T + b.G + b.K + b.N + b.R)/b.i) {
                return -1;
              }

              if((a.T + a.G + a.K + a.N + a.R)/a.i === (b.T + b.G + b.K + b.N + b.R)/b.i) {
                if(a.T/a.i > b.T/b.i) {
                  return 1;
                }

                if(a.T/a.i < b.T/b.i) {
                  return -1;
                }

                if(a.T/a.i === b.T/b.i) {
                  if(a.R/a.i > b.R/b.i) {
                    return 1;
                  }

                  if(a.R/a.i < b.R/b.i) {
                    return -1;
                  }
                }
              }
            }).reverse();

            io.emit('final ranking', sortedFinalRanking);
          }
        }
      }
    });
  };
};

//bigdata
//programowanie reaktywne - oparte o z jednej strony zdarzeniowa nature systemu, z drugiej strony oparte o
/*
  asynchronicznosc - wchodzi praktycznie do kazdego jezyka programowania. srodowiska, wszystko wlasciwie dzieje sie w sieci, wiec klientow
  w sieci jest masa
  wszystkie systemu, ktore sie licza sa oparte w jakis sposob o idee programowania asynchronicznego
  ES6 - javascript, promisy, mechanizm generatorow
  http
*/

httpServer.listen(8080, function () {
    console.log('Serwer HTTP działa na porcie 8080');
});

module.exports = app;
