var socket          = io();

$(document).ready(function() {
  /* jshint browser: true, globalstrict: true, devel: true */
  /* global io: false */
  "use strict";

  var start           = $("#start");
  var raceTable       = $("#raceTable");
  var judgeContainer  = $("#judgeContainer");
  var judgeTable      = $("#judgeTable");
  var judgeLogin      = $("#judgeLogin");
  var judgeIDValue    = $("#judgeID");
  var judgeIDSubmit   = $("#judgeIDSubmit");
  var judgingPanel    = $("#judgingPanel");

  //  var agreeJudge      = $("#agreeJudge");

  socket.emit('user connected');
  socket.emit('judgePage');

  socket.on('judge', function(data) {
    judgingPanel.hide();
    judgeContainer.append($('<div class="judgeInvite"><div>' +
                            '<h2>You have been chosen as a judge!</h2>' +
                            '<input type="submit" onclick="agree()" value="Agree">' +
                            '<input type="submit" onclick="decline()" value="Decline">' +
                            '</div></div>'));
    judgeContainer.show();
  });

  socket.on('prepare yourself', function(data) {
    $(".judgeInvite div").each(function() {
      $(this).remove();
    });

    var counter = 20;
    setInterval(function() {
      if(counter >= 0) {
        $(".judgeInvite").addClass('clock');
        $(".judgeInvite").html("<h2>Horse number " + data + " in " + counter + " seconds.</h2>");
      }

      if(counter === 0) {
        $(".judgeInvite").hide();
        socket.emit('let horse');
      }

      counter--;
    }, 1000);
  });

  socket.on('clear', function() {
    judgingPanel.show();
    $("#judgeLogin div").each(function() {
      $(this).remove();
    });

    $("#judgeTable div").each(function() {
      $(this).remove();
    });
  });

  socket.on('judge table', function(data) {
      $("#judgeLogin div").each(function() {
        $(this).remove();
      });

      $("#judgeTable div").each(function() {
        $(this).remove();
      });

      console.log(data);
      raceTable.hide();
      judgeContainer.hide();
      judgeLogin.append($('<div><h2>Enter your judge ID</h2><br />' +
          '<input type="password" id="judgeID" />' +
          '<input type="submit" id="judgeIDSubmit" value="Akceptuj" /></div>'
      ));
      judgeLogin.removeClass('hide');

      $("#judgeIDSubmit").click(function() {
        if(data.key.identyfikator == $("#judgeID").val()) {
          judgeLogin.addClass('hide');
          $("#judgeTable").append($('<div><h2>Horse race number: ' + data.horseNumber + '</h2>' +
                              '<span class="category">T</span> <input type="range" id="T" value="1" required min="1" max="20" onchange="updateT(this.value);"/><span id="valueT" class="rangeValue">1</span><br />' +
                              '<span class="category">G</span> <input type="range" id="G" value="1" required min="1" max="20" onchange="updateG(this.value);"/><span id="valueG" class="rangeValue">1</span><br />' +
                              '<span class="category">K</span> <input type="range" id="K" value="1" required min="1" max="20" onchange="updateK(this.value);"/><span id="valueK" class="rangeValue">1</span><br />' +
                              '<span class="category">N</span> <input type="range" id="N" value="1" required min="1" max="20" onchange="updateN(this.value);"/><span id="valueN" class="rangeValue">1</span><br />' +
                              '<span class="category">R</span> <input type="range" id="R" value="1" required min="1" max="20" onchange="updateR(this.value);"/><span id="valueR" class="rangeValue">1</span><br />' +
                              '<input type="submit" id="sendMarks" value="Send marks" /></div>'));
          judgeTable.show();
          $("#sendMarks").click(function() {
              var horseMarks = {
                judge: null,
                horseID: data.horseID,
                T: parseInt($("#T").val()),
                G: parseInt($("#G").val()),
                K: parseInt($("#K").val()),
                N: parseInt($("#N").val()),
                R: parseInt($("#R").val())
              };
              socket.emit('judgeMarks', horseMarks);
          });
        } else {
          alert("Bledny identyfikator!");
        }
      });
  });

  start.click(function() {
    socket.emit('race start', { start: true });
  });
});

function agree() {
  $(".judgeInvite div").each(function() {
    $(this).remove();
  });

  $(".judgeInvite").addClass('waitingFor');
  $(".judgeInvite").append($('<div><h2>Waiting for all judges confirmation.</h2></div>'));

  socket.emit('judgeAgreement');
}

function decline() {
  $(".judgeInvite").remove();
  judgeContainer.hide();
  socket.emit('judgeDecline');
}

function change(horseID) {
  var horseMarks = {
    judge: null,
    horseID: horseID,
    T: parseInt($("#T").val()),
    G: parseInt($("#G").val()),
    K: parseInt($("#K").val()),
    N: parseInt($("#N").val()),
    R: parseInt($("#R").val())
  };
  socket.emit('judgeMarks', horseMarks);
}

function updateT(value) {
  $("#valueT").html(value);
}

function updateG(value) {
  $("#valueG").html(value);
}

function updateK(value) {
  $("#valueK").html(value);
}

function updateN(value) {
  $("#valueN").html(value);
}

function updateR(value) {
  $("#valueR").html(value);
}
