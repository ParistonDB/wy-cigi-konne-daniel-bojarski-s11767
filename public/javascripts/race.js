var socket          = io();

$(document).ready(function() {
  /* jshint browser: true, globalstrict: true, devel: true */
  /* global io: false */
  "use strict";

  var start           = $("#start");
  var raceTable       = $("#raceTable");
  var judgeContainer  = $("#judgeContainer");
  var judgeTable      = $("#judgeTable");
  var lastMarks       = $("#lastMarks");
  var actualHorse     = $("#actualHorse");
  var finalRanking    = $("#finalRanking");
  var letHorse        = $("#letHorse").prop('disabled', true);
  var wForJudges      = $("#waitingForJudges");
  var raceInfo        = $("#raceInfo");

//  var agreeJudge      = $("#agreeJudge");

  start.click(function() {
    socket.emit('race start', { start: true });
    start.prop('disabled', true);
    wForJudges.hide();
    raceInfo.hide();
  });

  socket.on('waitingForJudges', function() {
    wForJudges.html("Waiting for all judges confirmation.");
    wForJudges.removeClass('hide');
    wForJudges.show();
  });

  letHorse.click(function() {
    socket.emit('prepare yourself');
    letHorse.prop('disabled', true);
  });

  socket.on('let horse unlock', function() {
    wForJudges.addClass('hide');
    letHorse.prop('disabled', false);
  });

  socket.on('let start unlock', function() {
    start.prop('disabled', false);
  });

  socket.on('race info', function(data) {
    raceInfo.html((data.activeHorses - data.activeLeftHorses) + " from " + data.activeHorses + " has ended the race.");
    raceInfo.removeClass('hide');
  });
});
