var socket          = io();

$(document).ready(function() {
/* jshint browser: true, globalstrict: true, devel: true */
/* global io: false */
"use strict";

var start           = $("#start");
var raceTable       = $("#raceTable");
var judgeContainer  = $("#judgeContainer");
var judgeTable      = $("#judgeTable");
var lastMarks       = $("#lastMarks");
var actualHorse     = $("#actualHorse");
var finalRanking    = $("#finalRanking");
var raceStatus      = $("#raceStatus");
var centerInfo      = $(".centerInfo");

socket.on('history', function(data) {
  if(data.isRace === true) {
    centerInfo.show();
    $("#lastMarks td, tr.marks").each(function() {
      $(this).remove();
    });
    actualHorse.html("<span class='currently'>currently running horse:</span> " + "<span class='actualHorse'>" + data.actualHorse.name + "</span>");
    actualHorse.show();
    for(var i = 0; i < data.judgeMarks.length; i++) {
      lastMarks.append($('<tr class="marks">' +
                         '<td>' + data.judgeMarks[i].T + '</td>' +
                         '<td>' + data.judgeMarks[i].G + '</td>' +
                         '<td>' + data.judgeMarks[i].K + '</td>' +
                         '<td>' + data.judgeMarks[i].N + '</td>' +
                         '<td>' + data.judgeMarks[i].R + '</td>'
                       ));
    }

    lastMarks.show();
    raceStatus.addClass('hide');
    $("#raceTable td, tr.horse").each(function() {
      $(this).remove();
    });

    for(i = 0; i < data.activeHorses.length; i++) {
      raceTable.append($('<tr class="horse">' +
                         '<td>' + data.activeHorses[i].name + '</td>' +
                         '<td>' + (i+1) + '</td>'
                       ));
    }
    raceTable.show();

    $("#finalRanking td, tr.ranking").each(function() {
      $(this).remove();
    });

    for(i = 0; i < data.finalRanking.length; i++) {
      finalRanking.append($('<tr class="ranking">' +
                         '<td>' + (i+1) + '</td>' +
                         '<td>' + data.finalRanking[i].name + '</td>' +
                         '<td>' + ((data.finalRanking[i].T + data.finalRanking[i].G + data.finalRanking[i].K + data.finalRanking[i].N + data.finalRanking[i].R)/(data.finalRanking[i].judgesCount)).toFixed(2) + '</td>' +
                         '<td>' + (data.finalRanking[i].T/data.finalRanking[i].judgesCount).toFixed(2) + '</td>' +
                         '<td>' + (data.finalRanking[i].R/data.finalRanking[i].judgesCount).toFixed(2) + '</td>'
                       ));
    }
    finalRanking.show();
  } else {
    raceStatus.html("There is no race right now");
    raceTable.hide();
    raceStatus.removeClass('hide');
    actualHorse.hide();
    lastMarks.hide();
    finalRanking.hide();
    centerInfo.hide();
  }
});

socket.on('print horses', function(data) {
  raceStatus.addClass('hide');
  $("#raceTable td, tr.horse").each(function() {
    $(this).remove();
  });

  for(var i = 0; i < data.length; i++) {
    raceTable.append($('<tr class="horse">' +
                       '<td>' + data[i].name + '</td>' +
                       '<td>' + (i+1) + '</td>'
                     ));
  }
  centerInfo.show();
  actualHorse.show();
  raceTable.show();
  lastMarks.show();
  finalRanking.show();
});

socket.on('last marks', function(data) {
  $("#lastMarks td, tr.marks").each(function() {
    $(this).remove();
  });
  actualHorse.html("<span class='currently'>currently running horse:</span> " + "<span class='actualHorse'>" + data.actualHorse.name + "</span>");
  for(var i = 0; i < data.judgeMarks.length; i++) {
    lastMarks.append($('<tr class="marks">' +
                       '<td>' + data.judgeMarks[i].T + '</td>' +
                       '<td>' + data.judgeMarks[i].G + '</td>' +
                       '<td>' + data.judgeMarks[i].K + '</td>' +
                       '<td>' + data.judgeMarks[i].N + '</td>' +
                       '<td>' + data.judgeMarks[i].R + '</td>'
                     ));
  }
});

socket.on('final ranking', function(data) {
  console.log(data);
  $("#finalRanking td, tr.ranking").each(function() {
    $(this).remove();
  });

  for(var i = 0; i < data.length; i++) {
    finalRanking.append($('<tr class="ranking">' +
                       '<td>' + (i+1) + '</td>' +
                       '<td>' + data[i].name + '</td>' +
                       '<td>' + ((data[i].T + data[i].G + data[i].K + data[i].N + data[i].R)/(data[i].judgesCount)).toFixed(2) + '</td>' +
                       '<td>' + (data[i].T/data[i].judgesCount).toFixed(2) + '</td>' +
                       '<td>' + (data[i].R/data[i].judgesCount).toFixed(2) + '</td>'
                     ));
  }
});
});
