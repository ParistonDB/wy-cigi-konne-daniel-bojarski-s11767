var express                 = require('express'),
    router                  = express.Router(),
    mongoose                = require('mongoose'),
    _                       = require('underscore'),
    users                   = require('./models/user'),
    horses                  = require('./models/horse');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index.html', { title: 'Express' });
});

router.get('/racestart', function(req, res, next) {
  res.render('racestart.html', { title: 'Start race' });
});

router.post('/racestart', function(req, res, next) {
  if(req.body.start === true) {
    horses.find(function(error, result) {
      if(error) console.log(error);
      else {
        req.session.user = result;
        console.log(result);
      }
    });
  }

  res.end();
});

module.exports = router;
