var mongoose                  = require('mongoose'),
    races                     = require('./race'),
    horse = mongoose.model('horses', new mongoose.Schema({
       _id:            String,
      name:            String,
judgesCount:            Number,
         T:            { type: Number, default: 0 },
         G:            { type: Number, default: null },
         K:            { type: Number, default: null },
         N:            { type: Number, default: null },
         R:            { type: Number, default: null },
      wins:            [{ type: String, ref: 'races'}]
    }));

module.exports = horse;
