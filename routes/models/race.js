var mongoose                  = require('mongoose'),
    horse                     = require('./horse'),
    race = mongoose.model('races', new mongoose.Schema({
      _id:            String,
     name:            String,
     date:            String,
   winner:            { type: String, ref: 'horse' }
    }));

module.exports = race;
