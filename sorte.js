var array = [
    { id: 1, i: 1, T: 2, G: 1, K: 2},        //<--- return this cause T+G+K = 6
    { id: 2, i: 2, T: 2, G: 2, K: 3},
    { id: 3, i: 2, T: 2, G: 1, K: 2}         //<--- return this cause T+G+K = 6
];

var sorted = array.sort(function(a, b) {
  if(a.T + a.G + a.K > b.T + b.G + b.K) {
    return 1;
  }

  if(a.T + a.G + a.K < b.T + b.G + b.K) {
    return -1;
  }

  if(a.T + a.G + a.K === b.T + b.G + b.K) {
    if(a.T/a.i > b.T/b.i) {
      return 1;
    }

    if(a.T/a.i < b.T/b.i) {
      return -1;
    }

    if(a.T/a.i === b.T/b.i) {
      if(a.R/a.i > b.R/b.i) {
        return 1;
      }

      if(a.R/a.i < b.R/b.i) {
        return -1;
      }
    }
  }
});
console.log(sorted);
