var _ = require('underscore');

var array = [
  { id: 1, judgeCount: 2, T: 2, G: 2, K: 2},
  { id: 2, judgeCount: 4, T: 3, G: 3, K: 3},
  { id: 3, judgeCount: 3, T: 2, G: 2, K: 2}
];

var sortedArray = _.sortBy(array, function(array) { return array.T + array.G + array.K; });
var sortedUniqArray = _.uniq(sortedArray, function(sortedArray) { return sortedArray.T + sortedArray.G + sortedArray.K; });
var pomuszmi = [];

console.log("Sorted array:");
console.log(sortedArray);
console.log("Sorted, uniq array:");
console.log(sortedUniqArray);
console.log("Difference between sorted array and sorted, uniq array:");
console.log(_.difference(sortedArray, sortedUniqArray));
console.log("The same sum T, G, K objects.");
console.log(
  _.each(sortedArray, function(value) {
    if((value.T + value.K + value.G) === 1) {
      pomuszmi.push(value);
    }
  })
);
console.log(pomuszmi);

console.log(_.filter(sortedArray, function(value, index) {
  return value.T === _.each(_.difference(sortedArray, sortedUniqArray).T);
}));
